package com.example.jpa_project.entity;

import javax.persistence.*;


@Entity
@Table(name="tbl_course_material")
public class CourseMaterial{
	
	@Id
	@SequenceGenerator( name = "course_material_sequence",sequenceName ="course_material_sequence",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator ="course_material_sequence")
	private Long courseMaterialId;
	
	private String url;
	
	@OneToOne(cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
	@JoinColumn(name ="course_id",referencedColumnName = "courseId")
	private Course course;

	
	//Getters and Setters
	
	public Long getCourseMaterialId() {
		return courseMaterialId;
	}

	public void setCourseMaterialId(Long courseMaterialId) {
		this.courseMaterialId = courseMaterialId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	
}
