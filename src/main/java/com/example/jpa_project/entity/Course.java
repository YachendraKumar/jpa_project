package com.example.jpa_project.entity;

import javax.persistence.*;

@Entity
@Table(name="tbl_course")
public class Course {

	@Id
	@SequenceGenerator( 	name ="course_sequence",sequenceName ="course_sequence",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator ="course_sequence" )
	private Long courseId;
	
	private String title;
	
	private Integer credit;
	
	
	//Getters and Setters

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCredit() {
		return credit;
	}

	public void setCredit(Integer credit) {
		this.credit = credit;
	}
	
	
	
	
	
}
