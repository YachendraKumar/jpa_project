package com.example.jpa_project.dao;

import com.example.jpa_project.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserDAO extends JpaRepository<User, UUID>{

	@Query(value="select * from tbl_user  where tbl_user.last_name = ?1 and tbl_user.first_name = ?2",nativeQuery = true)
	List<User> findByLastnameAndFirstname(String firstName, String lastName);
	
	
	@Query(value="select * from tbl_user  where tbl_user.last_name = :lastName or tbl_user.first_name = :firstName",nativeQuery = true)
	List<User> findByLastnameOrFirstname(String firstName, String lastName);

	@Query(value="select * from tbl_user  where tbl_user.active_status =:activeStatus",nativeQuery = true)
	List<User> findByActiveStatus(String activeStatus);

	@Query(value="select distinct * from tbl_user  where tbl_user.last_name = ?1 and tbl_user.first_name = ?2",nativeQuery = true)
	List<User> findDistinctByLastnameAndFirstname(String firstName, String lastName);

	@Query(value="select * from tbl_user  where tbl_user.age < ?1",nativeQuery = true)
	List<User> findByAgeLessThan(Integer age);

	@Query(value="select * from tbl_user  where tbl_user.age <= ?1",nativeQuery = true)
	List<User> findByAgeLessThanEqual(Integer age);

	@Query(value="select * from tbl_user  where tbl_user.age > ?1",nativeQuery = true)
	List<User> findByAgeGreaterThan(Integer age);

	@Query(value="select * from tbl_user  where tbl_user.age >= ?1",nativeQuery = true)
	List<User> findByAgeGreaterThanEqual(Integer age);

	@Query(value="select * from tbl_user  where tbl_user.first_name = :firstName",nativeQuery = true)
	List<User> findByFirstname(String firstName);

	@Query(value="select * from tbl_user  where tbl_user.last_name = :lastName",nativeQuery = true)
	List<User> findByLastname(String lastName);

	@Query(value="select * from tbl_user  where UPPER(tbl_user.first_name) = UPPER(?1)",nativeQuery = true)
	List<User> findByFirstnameIgnoreCase(String firstName);

	@Query(value="select * from tbl_user  where UPPER(tbl_user.last_name) = UPPER(?1)",nativeQuery = true)
	List<User> findByLastnameIgnoringCase(String lastName);

	@Query("select u from User u where u.firstName LIKE %:firstName%")
	List<User> findByFirstNameWithContains(@Param("firstName") String firstName);


	List<User> findByFirstNameContainsIgnoreCase(String firstName);


	List<User> findByLastNameContainsIgnoreCase(String lastName);

	@Query("select u from User u where u.firstName LIKE %:firstName%")
	List<User> findByFirstnameContaining(@Param("firstName") String firstName);

	@Query(value="select * from tbl_user  where tbl_user.last_name not null",nativeQuery = true)
	List<User> findByLastnameNotNull(String lastName);

	@Query("select u from User u where u.firstName LIKE  :firstName%")
	List<User> findByFirstnameStartingWith(@Param("firstName") String firstName);

	@Query("select u from User u where u.lastName LIKE  %:lastName")
	List<User> fetchByLastNameEndingWith(@Param("lastName") String lastName);


	List<User> findByFullNameContainsIgnoreCase(String fullName);
}
