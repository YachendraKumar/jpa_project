package com.example.jpa_project.dao;

import com.example.jpa_project.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@EnableJpaRepositories
@Repository
public interface DepartmentDAO extends JpaRepository<Department,UUID>{

	//case sensitive
	List<Department> findByLocation(String location);

	//ignore case
	List<Department> findByLocationIgnoreCase(String location);

	//case sensitive
	List<Department> findByDeptName(String deptName);

	//ignore case
	List<Department> findByDeptNameIgnoreCase(String deptName);

}
