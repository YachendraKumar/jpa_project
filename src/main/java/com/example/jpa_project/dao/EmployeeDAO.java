package com.example.jpa_project.dao;

import com.example.jpa_project.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EmployeeDAO extends JpaRepository<Employee, UUID>{

	List<Employee> findByFirstNameLikeIgnoreCase(String firstName);

	List<Employee> findByFirstNameContainsIgnoreCase(String firstName);

	List<Employee> findByFirstNameStartingWithIgnoreCase(String firstName);

	List<Employee> findByLastNameStartingWithIgnoreCase(String lastName);

	List<Employee> findByFirstNameEndingWithIgnoreCase(String firstName);

	List<Employee> findByLastNameEndingWithIgnoreCase(String lastName);

}
