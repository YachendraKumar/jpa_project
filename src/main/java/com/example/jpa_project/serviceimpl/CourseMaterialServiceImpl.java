package com.example.jpa_project.serviceimpl;

import com.example.jpa_project.dao.CourseDAO;
import com.example.jpa_project.dao.CourseMaterialDAO;
import com.example.jpa_project.entity.CourseMaterial;
import com.example.jpa_project.service.CourseMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseMaterialServiceImpl implements CourseMaterialService{

	@Autowired
	CourseMaterialDAO courseMaterialDAO;
	
	@Autowired
	CourseDAO courseDAO;




	@Override
	public Object saveAsCourseMaterial(CourseMaterial courseMaterial) {
		 return courseMaterialDAO.save(courseMaterial);
	}


}
