package com.example.jpa_project.serviceimpl;

import com.example.jpa_project.dao.DepartmentDAO;
import com.example.jpa_project.entity.Department;
import com.example.jpa_project.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	DepartmentDAO departmentDAO;
	
	@Override
	public Object saveDepartment(Department department) {
		return departmentDAO.save(department);
	}

	@Override
	public Optional<Department> fetchDeptById(UUID id) {
		return departmentDAO.findById(id);
	}

	@Override
	public List<Department> fetchDeptByList(Department department) {
		return departmentDAO.findAll(Sort.by("location"));
	}

	@Override
	public List<Department> fetchByLocation(String location) {
		return departmentDAO.findByLocation(location);
	}

	@Override
	public List<Department> fetchByLocationWithIgnoreCase(String location) {
		return departmentDAO.findByLocationIgnoreCase(location);
	}

	@Override
	public List<Department> fetchByDeptName(String deptName) {
		return departmentDAO.findByDeptName(deptName);
	}

	@Override
	public List<Department> fetchByDeptNameIgnoreCase(String deptName) {
		return departmentDAO.findByDeptNameIgnoreCase(deptName);
	}

}
