package com.example.jpa_project.serviceimpl;

import com.example.jpa_project.dao.UserDAO;
import com.example.jpa_project.entity.User;
import com.example.jpa_project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserDAO userDAO;
	
	@Override
	public Object saveData(User user) {
		return userDAO.save(user);
	}

	@Override
	public List<User> fetchList(User user) {
		return userDAO.findAll();
	}

	@Override
	public List<User> findByLastnameAndFirstname(String firstName, String lastName) {
		return userDAO.findByLastnameAndFirstname(firstName,lastName);
	}

	@Override
	public List<User> findByLastnameOrFirstname(String firstName, String lastName) {
		return userDAO.findByLastnameOrFirstname(firstName,lastName);
	}

	@Override
	public List<User> fetchByStatus(String activeStatus) {
		return userDAO.findByActiveStatus(activeStatus);
	}

	@Override
	public List<User> fetchDistinctByLastnameAndFirstname(String firstName, String lastName) {
		return userDAO.findDistinctByLastnameAndFirstname(firstName,lastName);
	}

	@Override
	public List<User> fetchByAgeLessThan(Integer age) {
		return userDAO.findByAgeLessThan(age);
	}

	@Override
	public List<User> fetchByAgeLessThanEqual(Integer age) {
		return userDAO.findByAgeLessThanEqual(age);

	}

	@Override
	public List<User> fetchByAgeGreaterThan(Integer age) {
		return userDAO.findByAgeGreaterThan(age);
	}

	@Override
	public List<User> fetchByAgeGreaterThanEqual(Integer age) {
		return userDAO.findByAgeGreaterThanEqual(age);
	}

	@Override
	public List<User> fetchByFirstname(String firstName) {
		return userDAO.findByFirstname(firstName);
	}

	@Override
	public List<User> fetchByLastname(String lastName) {
		return userDAO.findByLastname(lastName);
	}

	@Override
	public List<User> fetchByFirstnameIgnoreCase(String firstName) {
		return userDAO.findByFirstnameIgnoreCase(firstName);
	}

	@Override
	public List<User> fetchByLastnameIgnoringCase(String lastName) {
		return userDAO.findByLastnameIgnoringCase(lastName);
	}

	@Override
	public List<User> fetchByFirstNameWithContains(String firstName) {
		return userDAO.findByFirstNameWithContains(firstName);
	}

	@Override
	public List<User> fetchByFirstNameContainsIgnoreCase(String firstName) {
		return userDAO.findByFirstNameContainsIgnoreCase(firstName);
	}

	@Override
	public List<User> fetchByLastNameContainsIgnoreCase(String lastName) {
		return userDAO.findByLastNameContainsIgnoreCase(lastName);
	}

	@Override
	public List<User> fetchByFirstnameContaining(String firstName) {
		return userDAO.findByFirstnameContaining(firstName);
	}

	@Override
	public Optional<User> fetchById(UUID id) {
		return userDAO.findById(id);
	}

	@Override
	public Page<User> fetchListByPagenation(User user, int pageNumber, int pageSize) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("userId"));
		Page<User> cores = userDAO.findAll(pageable);
		return cores;
	}

	@Override
	public List<User> fetchByFirstnameStartingWith(String firstName) {
		return userDAO.findByFirstnameStartingWith(firstName);
	}

	@Override
	public List<User> fetchByLastNameEndingWith(String lastName) {
		return userDAO.fetchByLastNameEndingWith(lastName);
	}

	@Override
	public List<User> fetchByFullNameContainsIgnoreCase(String fullName) {
		return userDAO.findByFullNameContainsIgnoreCase(fullName);
	}

}
