package com.example.jpa_project.serviceimpl;

import com.example.jpa_project.dao.CourseDAO;
import com.example.jpa_project.entity.Course;
import com.example.jpa_project.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	CourseDAO courseDAO;
	
	@Override
	public Object saveCourseData(Course course) {
		return courseDAO.save(course);
	}

}
