package com.example.jpa_project.serviceimpl;

import com.example.jpa_project.dao.EmployeeDAO;
import com.example.jpa_project.entity.Employee;
import com.example.jpa_project.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeDAO employeeDAO;
	
	@Override
	public Object saveByData(Employee employee) {
		return employeeDAO.save(employee);
	}

	@Override
	public List<Employee> fetchByFirstnameLike(String firstName) {
		return employeeDAO.findByFirstNameLikeIgnoreCase(firstName);
	}

	@Override
	public List<Employee> fetchByFirstNameContainsIgnoreCase(String firstName) {
		return employeeDAO.findByFirstNameContainsIgnoreCase(firstName);
	}

	@Override
	public List<Employee> fetchByFirstNameStartingWithIgnoreCase(String firstName) {
		return employeeDAO.findByFirstNameStartingWithIgnoreCase(firstName);
	}

	@Override
	public List<Employee> fetchBylastNameStartingWithIgnoreCase(String lastName) {
		return employeeDAO.findByLastNameStartingWithIgnoreCase(lastName);
	}

	@Override
	public List<Employee> fetchByFirstNameEndingWithIgnoreCase(String firstName) {
		return employeeDAO.findByFirstNameEndingWithIgnoreCase(firstName);
	}

	@Override
	public List<Employee> fetchBylastNameEndingWithIgnoreCase(String lastName) {
		return employeeDAO.findByLastNameEndingWithIgnoreCase(lastName);
	}

	@Override
	public List<Employee> fetchByList(Employee employee) {
		return employeeDAO.findAll();
	}

	@Override
	public Optional<Employee> fetchById(UUID id) {
		return employeeDAO.findById(id);
	}

	@Override
	public List<Employee> multipleSearchOperation(Employee employee) {
		List<Employee> searchOperation = new ArrayList<>();
		if(employee.getFirstName() != null) {
			searchOperation = employeeDAO.findByFirstNameLikeIgnoreCase(employee.getFirstName());
		}
		else if(employee.getFirstName() != null) {
			searchOperation = employeeDAO.findByFirstNameContainsIgnoreCase(employee.getFirstName());
		}
		else if(employee.getFirstName() != null) {
			searchOperation = employeeDAO.findByFirstNameStartingWithIgnoreCase(employee.getFirstName());
		}
		else if(employee.getLastName() != null) {
			searchOperation = employeeDAO.findByLastNameStartingWithIgnoreCase(employee.getLastName());
		}
		else if(employee.getFirstName() != null) {
			searchOperation = employeeDAO.findByFirstNameEndingWithIgnoreCase(employee.getFirstName());
		}
		else if(employee.getLastName() != null) {
			searchOperation = employeeDAO.findByLastNameEndingWithIgnoreCase(employee.getLastName());
		}
		return searchOperation;
	}

}
