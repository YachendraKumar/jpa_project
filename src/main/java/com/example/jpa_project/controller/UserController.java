package com.example.jpa_project.controller;

import com.example.jpa_project.entity.User;
import com.example.jpa_project.service.UserService;
import com.example.jpa_project.util.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@PostMapping("/save")
	public CommonResponse saveData(@RequestBody User user) {
		return new CommonResponse(1,"User Saved",userService.saveData(user),true,true);
	}
	
	@GetMapping("/list")
	public CommonResponse fetchList(@RequestBody User user) {
		List<User> fetchByList = userService.fetchList(user);
		return new CommonResponse(fetchByList.size(),"Fetched List Sucessfully",fetchByList,true,true);
	}
	
	//Fetch By FirstNameAndLastName
	@GetMapping("/firstNameAndLastName/{firstName}/{lastName}")
	public CommonResponse findByLastnameAndFirstname(@PathVariable String firstName,@PathVariable String lastName) {
		List<User> frstAndlstName = userService.findByLastnameAndFirstname(firstName,lastName);
		return new CommonResponse(frstAndlstName.size(),"Fetched",frstAndlstName,true,true);
	}
	
	//Fetch By FirstNameOrLastName
	@GetMapping("/firstNameOrLastName")
	public CommonResponse findByLastnameOrFirstname(@RequestParam(required = false) String firstName,@RequestParam(required = false) String lastName) {
		List<User> frstOrlstName = userService.findByLastnameOrFirstname(firstName,lastName);
		return new CommonResponse(frstOrlstName.size(),"Fetched",frstOrlstName,true,true);
	}
	
	//fetch By Status
	@GetMapping("/activestatus")
	public CommonResponse fetchByStatus(@RequestParam String activeStatus) {
		List<User> fetchByStatus = userService.fetchByStatus(activeStatus);
		return new CommonResponse(fetchByStatus.size(),"Status Fetched",fetchByStatus,true,true);
	}
	
	//Distinct FirstName And LastName
	@GetMapping("/distnictFirstNameAndLastName/{firstName}/{lastName}")
	public CommonResponse fetchDistinctByLastnameAndFirstname(@PathVariable String firstName,@PathVariable String lastName) {
		List<User> distinct = userService.fetchDistinctByLastnameAndFirstname(firstName,lastName);
		return new CommonResponse(distinct.size(),"Fetched",distinct,true,true);
	}
	
	//Age LessThan
	@GetMapping("/ageLessThan/{age}")
	public CommonResponse fetchByAgeLessThan(@PathVariable("age") Integer age) {
		List<User> fetchByAge = userService.fetchByAgeLessThan(age);
		return new CommonResponse(fetchByAge.size(),"Status Fetched",fetchByAge,true,true);
	}
	
	//Age LessThanEqual
	@GetMapping("/ageLessThanEqualTo/{age}")
	public CommonResponse fetchByAgeLessThanEqual(@PathVariable("age") Integer age) {
		List<User> fetchByAge = userService.fetchByAgeLessThanEqual(age);
		return new CommonResponse(fetchByAge.size(),"Status Fetched",fetchByAge,true,true);
	}
	//Age GreaterThan
	@GetMapping("/agegreaterthan/{age}")
	public CommonResponse fetchByAgeGreaterThan(@PathVariable("age") Integer age) {
		List<User> fetchByAge = userService.fetchByAgeGreaterThan(age);
		return new CommonResponse(fetchByAge.size(),"Status Fetched",fetchByAge,true,true);
	}
	//Age GreaterThanEqual
	@GetMapping("/agegreaterthanequal/{age}")
	public CommonResponse fetchByAgeGreaterThanEqual(@PathVariable("age") Integer age) {
		List<User> fetchByAge = userService.fetchByAgeGreaterThanEqual(age);
		return new CommonResponse(fetchByAge.size(),"Status Fetched",fetchByAge,true,true);
	}
	
	//Find By FirstName
	@GetMapping("/findByFirstName/{firstName}")
	public CommonResponse fetchByFirstname(@PathVariable("firstName") String firstName) {
		List<User> distinct = userService.fetchByFirstname(firstName);
		return new CommonResponse(distinct.size(),"Fetched",distinct,true,true);
	}
	
	//Find By LastName
		@GetMapping("/findByLastName/{lastName}")
		public CommonResponse fetchByLastname(@PathVariable("lastName") String lastName) {
			List<User> distinct = userService.fetchByLastname(lastName);
			return new CommonResponse(distinct.size(),"Fetched",distinct,true,true);
		}
		
		//Find By FirstNameIgnoringCase
		@GetMapping("/findByFirstNameIgnoreCase/{firstName}")
		public CommonResponse fetchByFirstnameIgnoreCase(@PathVariable("firstName") String firstName) {
			List<User> distinct = userService.fetchByFirstnameIgnoreCase(firstName);
			return new CommonResponse(distinct.size(),"Fetched",distinct,true,true);
		}
		
		//FindByLastNameIgnoringCase
		@GetMapping("/findByLastNameIgnoringCase/{lastName}")
		public CommonResponse fetchByLastnameIgnoringCase(@PathVariable("lastName") String lastName) {
			List<User> distinct = userService.fetchByLastnameIgnoringCase(lastName);
			return new CommonResponse(distinct.size(),"Fetched",distinct,true,true);
		}
		
		//Fetch By FirstNameContains
		@GetMapping("/firstNameWithContains/{firstName}")
		public CommonResponse fetchByFirstNameWithContains(@PathVariable String firstName) {
			List<User> frstAndlstName = userService.fetchByFirstNameWithContains(firstName);
			return new CommonResponse(frstAndlstName.size(),"Fetched",frstAndlstName,true,true);
		}
		
		//Fetch By firstNameWithContainsIgnoreCase
		@GetMapping("/firstNameWithContainsIgnoreCase/{firstName}")
		public CommonResponse fetchByFirstNameContainsIgnoreCase(@PathVariable("firstName") String firstName) {
			List<User> firstNameContainsIgnoreCase = userService.fetchByFirstNameContainsIgnoreCase(firstName);
			return new CommonResponse(firstNameContainsIgnoreCase.size(),"Fetched",firstNameContainsIgnoreCase,true,true);
		}
		
			//Fetch By firstNameWithContainsIgnoreCase
				@GetMapping("/fullNameWithContainsIgnoreCase/{fullName}")
				public CommonResponse fetchByFullNameContainsIgnoreCase(@PathVariable("fullName") String fullName) {
					List<User> fullNameContainsIgnoreCase = userService.fetchByFullNameContainsIgnoreCase(fullName);
					return new CommonResponse(fullNameContainsIgnoreCase.size(),"Fetched",fullNameContainsIgnoreCase,true,true);
				}
		
		//Fetch By lastNameWithContainsIgnoreCase
		@GetMapping("/lastNameWithContainsIgnoreCase/{lastName}")
		public CommonResponse fetchByLastNameContainsIgnoreCase(@PathVariable("lastName") String lastName) {
			List<User> lastNameContainsIgnoreCase = userService.fetchByLastNameContainsIgnoreCase(lastName);
			return new CommonResponse(lastNameContainsIgnoreCase.size(),"Fetched",lastNameContainsIgnoreCase,true,true);
		}
		
		
		//Find By FirstNameContains
		@GetMapping("/fetchByFirstnameContaining/{firstName}")
		public CommonResponse fetchByFirstnameContaining(@PathVariable("firstName") String firstName) {
			List<User> frstAndlstName = userService.fetchByFirstnameContaining(firstName);
			return new CommonResponse(frstAndlstName.size(),"Fetched",frstAndlstName,true,true);
		}
		
		//FindBy UUID findById
		@GetMapping("/fetchById/{id}")
		public CommonResponse fetchById(@PathVariable("id") UUID id) {
			Optional<User> fetchByList = userService.fetchById(id);
			return new CommonResponse(1,"Fetched By Id Sucessfully",fetchByList,true,true);
		}
		
		//List with pagination
		@GetMapping("/list/pagenation")
		public CommonResponse fetchListByPagenation(@RequestBody User user ,int pageNumber,@RequestParam(required = false) int pageSize) {
			Page<User> fetchByList = userService.fetchListByPagenation(user,pageNumber-1,pageSize);
			return new CommonResponse(fetchByList.getSize(),"Fetched List Sucessfully",fetchByList,true,true);
		}
		
		//Firsstname  
		@GetMapping("fetchByFirstnameStartingWith/{firstName}")
		public CommonResponse fetchByFirstnameStartingWithIgnoreCase(@PathVariable("firstName") String firstName) {
			List<User> firstNameStartingWith = userService.fetchByFirstnameStartingWith(firstName);
			return new CommonResponse(firstNameStartingWith.size(),"Fetched",firstNameStartingWith,true,true);
		}
		
		@GetMapping("/lastNameEndingWith/{lastName}")
		public CommonResponse fetchByLastNameEndingWith(@PathVariable("lastName") String lastName) {
			List<User> lastNameEndingWith = userService.fetchByLastNameEndingWith(lastName);
			return new CommonResponse(lastNameEndingWith.size(),"Fetched",lastNameEndingWith,true,true);
		}

}
