package com.example.jpa_project.controller;

import com.example.jpa_project.entity.Course;
import com.example.jpa_project.service.CourseService;
import com.example.jpa_project.util.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/coursecontroller")
public class CourseController {

	@Autowired
	CourseService courseService;
	
	@PostMapping("/save")
	public CommonResponse saveCourseData(@RequestBody Course course) {
		return new CommonResponse(1,"saved",courseService.saveCourseData(course),true,true);
	}
}
