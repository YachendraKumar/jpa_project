package com.example.jpa_project.controller;

import com.example.jpa_project.entity.Department;
import com.example.jpa_project.service.DepartmentService;
import com.example.jpa_project.util.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/Jpa/deptController")
public class DepartmentController {

	@Autowired
	DepartmentService departmentService;
	 
	
	//save data on a single mode
	@PostMapping("/save")
	public CommonResponse saveDepartment(@RequestBody Department department) {
		return new CommonResponse(1,"true",departmentService.saveDepartment(department),true,true);
	}
	
	
	//Getting the data By using findById
	@GetMapping("/list/{id}")
	public CommonResponse fetchDeptById(@PathVariable("id") UUID id) {
		Optional<Department> deptById = departmentService.fetchDeptById(id);
		return new CommonResponse(1,"true",deptById,true,true);
	}
	
	
	//Getting the list Using FindByAll
	@GetMapping("/list")
	public CommonResponse fetchDeptByList(@RequestBody Department department ) {
		List<Department> fetchByList = departmentService.fetchDeptByList(department);
		return new CommonResponse(fetchByList.size(),"true",fetchByList,true,true);
	}
	
	
	//Find By location **CASE SENSITIVE**
	@GetMapping("/list/location/{location}")
	public CommonResponse fetchByLocation(@PathVariable("location") String location) {
		List<Department> findByLocation = departmentService.fetchByLocation(location);
		String message = "Fetched Data Based On Location "+location;
		return new CommonResponse(findByLocation.size(),message,findByLocation,true,true);
	}
	
	//Find By location **Ignore Case**
	@GetMapping(value ="/list/location/ignorecase/{location}")
	public CommonResponse fetchByLocationWithIgnoreCase(@PathVariable("location") String location) {
		List<Department> findByLocationIgnoreCase = departmentService.fetchByLocationWithIgnoreCase(location);
		String script = "Fetched Data Based On Location "+location;
		return new CommonResponse(findByLocationIgnoreCase.size(),script,findByLocationIgnoreCase,true,true);
	}
	
	
	//Find By DepartmentName ***CASE SENSITIVE***
	@GetMapping("/fetch/{name}")
	public CommonResponse fetchByDeptName(@PathVariable("name") String deptName) {
		List<Department> fetchDeptName = departmentService.fetchByDeptName(deptName);
		String mess = "Fetched Data Based On DepartmentName "+deptName;
		return new CommonResponse(fetchDeptName.size(),mess,fetchDeptName,true,true);
	}
	
	//Find By DepartmentName **Ignore Case**
	@GetMapping("/fetch/ignorecase/{name}")
	public CommonResponse fetchByDeptNameIgnoreCase(@PathVariable("name") String deptName) {
		List<Department> fetchDeptNameIgnoreCase = departmentService.fetchByDeptNameIgnoreCase(deptName);
		String mess = "Fetched Data Based On DepartmentName "+deptName;
		return new CommonResponse(fetchDeptNameIgnoreCase.size(),mess,fetchDeptNameIgnoreCase,true,true);
	}
	

}
