package com.example.jpa_project.controller;

import com.example.jpa_project.entity.Employee;
import com.example.jpa_project.service.EmployeeService;
import com.example.jpa_project.util.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@PostMapping("/save")
	public CommonResponse saveByData(@RequestBody Employee employee) {
		return new CommonResponse(1,"saved",employeeService.saveByData(employee),true,true);
	}
	//findByFirstNameLike
	@GetMapping("/findByFirstNameLike/{firstName}")
	public CommonResponse fetchByFirstnameLike(@PathVariable("firstName") String firstName) {
		List<Employee> fetchByFirstnameLike = employeeService.fetchByFirstnameLike(firstName);
		return new CommonResponse(fetchByFirstnameLike.size(),"fetchBy FirstName",fetchByFirstnameLike,true,true);
	}
	
	//findByFirstNameContainsIgnoreCase
	
	@GetMapping("/findByFirstNameContainsIgnoreCase/{firstName}")
	public CommonResponse fetchByFirstNameContainsIgnoreCase(@PathVariable("firstName") String firstName) {
		List<Employee> fetchByFirstnameLike = employeeService.fetchByFirstNameContainsIgnoreCase(firstName);
		return new CommonResponse(fetchByFirstnameLike.size(),"fetchBy FirstName",fetchByFirstnameLike,true,true);
	}
	
	@GetMapping("/findByFirstNameStartingWithIgnoreCase/{firstName}")
	public CommonResponse fetchByFirstNameStartingWithIgnoreCase(@PathVariable("firstName") String firstName) {
		List<Employee> fetchByFirstnameLike = employeeService.fetchByFirstNameStartingWithIgnoreCase(firstName);
		return new CommonResponse(fetchByFirstnameLike.size(),"fetchBy FirstName",fetchByFirstnameLike,true,true);
	}
	
	@GetMapping("/findByLastNameStartingWithIgnoreCase/{lastName}")
	public CommonResponse fetchBylastNameStartingWithIgnoreCase(@PathVariable("lastName") String lastName) {
		List<Employee> fetchByLastnameStarting = employeeService.fetchBylastNameStartingWithIgnoreCase(lastName);
		return new CommonResponse(fetchByLastnameStarting.size(),"fetchBy LastName",fetchByLastnameStarting,true,true);
	}
	
	@GetMapping("/findByFirstNameEndingWithIgnoreCase/{firstName}")
	public CommonResponse fetchByFirstNameEndingWithIgnoreCase(@PathVariable("firstName") String firstName) {
		List<Employee> fetchByFirstnameEndingWith = employeeService.fetchByFirstNameEndingWithIgnoreCase(firstName);
		return new CommonResponse(fetchByFirstnameEndingWith.size(),"fetchBy FirstName",fetchByFirstnameEndingWith,true,true);
	}
	
	@GetMapping("/findByLastNameEndingWithIgnoreCase/{lastName}")
	public CommonResponse fetchBylastNameEndingWithIgnoreCase(@PathVariable("lastName") String lastName) {
		List<Employee> fetchByLastnameEndingWith = employeeService.fetchBylastNameEndingWithIgnoreCase(lastName);
		return new CommonResponse(fetchByLastnameEndingWith.size(),"fetchBy LastName",fetchByLastnameEndingWith,true,true);
	}
	
	@GetMapping("/list")
	public CommonResponse fetchByList(@RequestBody Employee employee ) {
		List<Employee> list = employeeService.fetchByList(employee);
		return new CommonResponse(list.size(),"List Fetched",list,true,true);
	}
	
	@GetMapping("/findById/{id}")
	public CommonResponse fetchById(@PathVariable("id") UUID id) {
		Optional<Employee> fetchById = employeeService.fetchById(id);
		return new CommonResponse(1,"FetchBy UUID",fetchById,true,true);
	}
	
	@GetMapping("/multipleSearchOperation")
	public CommonResponse multipleSearchOperation(@RequestBody Employee employee ) {
		List<Employee> list = employeeService.multipleSearchOperation(employee);
		return new CommonResponse(list.size(),"Fetched Requirement",list,true,true);
	}
}
