package com.example.jpa_project.service;

import com.example.jpa_project.entity.Course;

public interface CourseService {

	Object saveCourseData(Course course);

}
