package com.example.jpa_project.service;

import com.example.jpa_project.entity.User;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

	Object saveData(User user);

	List<User> fetchList(User user);

	List<User> findByLastnameAndFirstname(String firstName, String lastName);

	List<User> findByLastnameOrFirstname(String firstName, String lastName);

	List<User> fetchByStatus(String activeStatus);

	List<User> fetchDistinctByLastnameAndFirstname(String firstName, String lastName);

	List<User> fetchByAgeLessThan(Integer age);

	List<User> fetchByAgeLessThanEqual(Integer age);

	List<User> fetchByAgeGreaterThan(Integer age);

	List<User> fetchByAgeGreaterThanEqual(Integer age);

	List<User> fetchByFirstname(String firstName);

	List<User> fetchByLastname(String lastName);

	List<User> fetchByFirstnameIgnoreCase(String firstName);

	List<User> fetchByLastnameIgnoringCase(String lastName);

	List<User> fetchByFirstNameWithContains(String firstName);

	List<User> fetchByFirstNameContainsIgnoreCase(String firstName);

	List<User> fetchByLastNameContainsIgnoreCase(String lastName);

	List<User> fetchByFirstnameContaining(String firstName);

	Optional<User> fetchById(UUID id);

	Page<User> fetchListByPagenation(User user, int pageNumber, int pageSize);

	List<User> fetchByFirstnameStartingWith(String firstName);

	List<User> fetchByLastNameEndingWith(String lastName);

	List<User> fetchByFullNameContainsIgnoreCase(String fullName);

}
