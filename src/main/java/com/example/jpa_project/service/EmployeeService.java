package com.example.jpa_project.service;

import com.example.jpa_project.entity.Employee;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EmployeeService {

	Object saveByData(Employee employee);

	List<Employee> fetchByFirstnameLike(String firstName);

	List<Employee> fetchByFirstNameContainsIgnoreCase(String firstName);

	List<Employee> fetchByFirstNameStartingWithIgnoreCase(String firstName);

	List<Employee> fetchBylastNameStartingWithIgnoreCase(String lastName);

	List<Employee> fetchByFirstNameEndingWithIgnoreCase(String firstName);

	List<Employee> fetchBylastNameEndingWithIgnoreCase(String lastName);

	List<Employee> fetchByList(Employee employee);

	Optional<Employee> fetchById(UUID id);

	List<Employee> multipleSearchOperation(Employee employee);

}
