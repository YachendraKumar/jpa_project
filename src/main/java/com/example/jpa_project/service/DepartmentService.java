package com.example.jpa_project.service;

import com.example.jpa_project.entity.Department;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DepartmentService {

	Object saveDepartment(Department department);

	Optional<Department> fetchDeptById(UUID id);

	List<Department> fetchDeptByList(Department department);

	List<Department> fetchByLocation(String location);

	List<Department> fetchByLocationWithIgnoreCase(String location);

	List<Department> fetchByDeptName(String deptName);

	List<Department> fetchByDeptNameIgnoreCase(String deptName);


}
